[![install size](https://packagephobia.now.sh/badge?p=maparama)](https://packagephobia.now.sh/result?p=maparama)

# maparama.js

Library that helps **me** with the creation of new leaflet maps.

maparama is a global variable created with browserify’s --standalone option

```
"dependencies": {
  "@turf/turf": "^5.1.6",
  "chroma-js": "^2.0.3",
  "geostats": "^1.7.0",
  "jquery": "^3.3.1",
  "leaflet": "^1.4.0",
  "leaflet-modal": "^0.1.5"
}
```

## Download:

[maparama-v0.3.0.zip](https://gitlab.com/sigon/maparama/-/archive/v0.3.0/maparama-v0.3.0.zip)

[maparama-v0.3.0.tar](https://gitlab.com/sigon/maparama/-/archive/v0.3.0/maparama-v0.3.0.tar)

There is a copy of the release files in /dist folder

## Using a CDN Version:

Just place this on your HTML:

```html
<link rel="stylesheet" href="https://unpkg.com/leaflet@1.4.0/dist/leaflet.css" />
<link rel="stylesheet" href="https://unpkg.com/maparama@0.3.0/dist/lib/leaflet-modal/leaflet.modal.css" />
<link rel="stylesheet" href="https://unpkg.com/maparama@0.3.0/dist/css/geostats.css" />
<link rel="stylesheet" href="https://unpkg.com/maparama@0.3.0/dist/css/style.css" />
<script src="https://unpkg.com/maparama@0.3.0/dist/maparama.js"></script>
```

## Usage:

maparama-skeketon browser ready:

download only the dist folder (maparama-skeketon):

[dist](https://www.jsdelivr.com/package/npm/maparama?path=dist)

```
dist/
  ├── css
  │   ├── geostats.css
  │   └── style.css
  ├── data
  │   └── polygons.geojson
  ├── lib
  │   ├── leaflet-1.4.0
  │   │   ├── css
  │   │   │   └── leaflet.css
  │   │   └── images
  │   └── leaflet-modal
  │       └── leaflet.modal.css
  ├── maparama.js
  ├── maparama.min.js
  └── polygons-example.html
```

npm:

```bash
# install maparama.js in some project folder
$ npm install maparama

# run this script to create a new folder with only the
# necessary files to execute a maparama.js proyect in the browser.
$ npm run skeleton

# after that you can delete the folder where maparama package was installed
```

## Docs:

[Documentation](https://gitlab.com/sigon/maparama/wikis/Documentation)

## Author

Maparama.js is written by [sigon](https://sigon.gitlab.io/)

## License

Release under GNU license
