## Changelog

### 0.3.0
* add leaflet-modal to handle popups nicely
* use dataset metadata to get map info
* added [joinData()](https://gitlab.com/sigon/maparama/wikis/Documentation#joindata)

### 0.2.1
* move lib folder to dist
* add link to maparama-skeleton for easy download of dist folder (zip/tar)

### 0.2.0
* added script to create maparama.js proyect folder

### 0.1.0
* added [addBasemap()](https://gitlab.com/sigon/maparama/wikis/Documentation#addbasemap)
* added [addMapInfo()](https://gitlab.com/sigon/maparama/wikis/Documentation#addmapinfo)
* added [addLogo()](https://gitlab.com/sigon/maparama/wikis/Documentation#addlogo)
* added [addGeojson()](https://gitlab.com/sigon/maparama/wikis/Documentation#addgeojson)
