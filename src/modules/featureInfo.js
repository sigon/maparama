// src/modules/featureInfo.js
// highlight feature on hover and show info on a fixed modal.

var L = require('leaflet')
var modal = require('./modal')

var featureInfo = {
  info: L.control()
}

featureInfo.info.onAdd = function (map) {
  this._div = L.DomUtil.create('div', 'info') // create a div with a class "info"
  featureInfo.update()
  return this._div
}

featureInfo.update = function (e) {
  var content = (e ? modal.getTimeSeriesModal(e) : '')
  featureInfo.info._div.innerHTML = content
}

module.exports = featureInfo
