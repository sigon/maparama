// src/modules/add-geojson.js

var $ = require('jquery')
var L = require('leaflet')
/* global M */

var modalModule = require('./modal')
var dataBreaksModule = require('./choropleth-maps/data-breaks')

var addGeojson = {
  dataName: '',
  data: {},
  dataOptions: {},
  dataClassification: {
    breaks: [],
    colors: [],
    legend: '',
    ranges: []
  },
  dataTimeSeries: {},
  addFeature: function (map, data) {
    var geojsonLfeature = L.geoJson(data, {
      style: styleData,
      pointToLayer: function (feature, latlng) {
        return new L.CircleMarker(latlng)
      },
      // filter: pointFilter,
      onEachFeature: function (feature, layer) {
        // addPopUp(feature, layer)
        layer.on({
          mouseover: M.hover.highlightFeature,
          mouseout: M.hover.resetHighlight,
          click: M.featureInfoAction === 'popup' ? modalModule.fire : M.chart.generateChart
        })
      }
    })
    M.geojsonObject = geojsonLfeature
    $('#mySpinner').removeClass('spinner')
    geojsonLfeature.addTo(map)
  },
  getFile: function (map, geojsonURL, options) {
    $('#mySpinner').addClass('spinner')
    $.getJSON(geojsonURL, function (data) {
      addGeojson.addFile(map, data, options)
    })
  },
  updateTimeSeries: function () {
    M.geojsonObject.clearLayers() // remove leaflet layer
    $('.geostats-legend').html('') // remove legend html
    this.addFile(M.mapObject, this.data, this.dataOptions) // add geojson layer
  },
  addFile: function (map, data, options) {
    this.dataName = options ? options.choroplethData : ''
    this.data = data
    this.dataOptions = options
    if (options) {
      M.featureInfoAction = options.featureInfo ? options.featureInfo : 'popup'
    }
    $('#mySpinner').addClass('spinner')
    // three options:
    // 1 no choropleth
    // 2 choropleth map one value (options.choroplethData()
    // 3 choropleth map time series (options.choroplethData & options.timeseries)
    if (options) {
      if (options.choroplethData && !options.timeseries) {
        console.log('#2 choropleth map one value')
        dataBreaksModule.getBreaks(data.features, options, function (dClassif) {
          addGeojson.dataClassification = dClassif
          $(dClassif.legend).insertBefore('.mapInfo-ctrl')
          M.mapInfo.addColor()
          addGeojson.addFeature(map, data)
        })
      } else if (options.choroplethData && options.timeseries) {
        console.log('#3 choropleth map time series')
        addGeojson.dataTimeSeries = options.timeseries
        dataBreaksModule.getBreaks(data.features, options, function (dClassif) {
          addGeojson.dataClassification = dClassif
          $(dClassif.legend).insertBefore('.mapInfo-ctrl')
          M.mapInfo.addColor()
          addGeojson.addFeature(map, data)
        })
      // no choropleth map:
      } else {
        console.log('No choropleth Data on addGeojson ()')
        $('#mySpinner').removeClass('spinner')
      }
    } else {
      console.log('#1 no choropleth')
      addGeojson.addFeature(map, data)
      M.mapInfo.addColor()
    }
  }
}
module.exports = addGeojson

function styleData (feature) {
  var value
  function isCurrentTime (element) {
    return element[timeField] === currentTime
  }
  // If given a choroplethData value use a color scheme
  if (!$.isEmptyObject(addGeojson.dataTimeSeries.arr)) {
    console.log('is NOT empty datatimes')
    var timeField = addGeojson.dataTimeSeries.timeField
    var currentTime = addGeojson.dataTimeSeries.current
    var timeValuesArray = feature.properties[addGeojson.dataName]
    var index = timeValuesArray.findIndex(isCurrentTime)
    value = addGeojson.dataName ? timeValuesArray[index][addGeojson.dataName]
      : null
  } else {
    value = addGeojson.dataName ? feature.properties[addGeojson.dataName]
      : null
  }

  if (feature.geometry.type === 'Point') {
    return {
      radius: 5,
      fillColor: '#ff7800',
      weight: 1,
      opacity: 0.2,
      fillOpacity: 0.8
    }
  } else {
    return {
      // color: value ? getColour(feature) : 'green',
      fillOpacity: 0.6,
      opacity: 0.9,
      weight: 0.9,
      fillColor: value ? getColour(feature, value) : 'green'
    }
  }
}

function getColour (feature, value) {
  var color = addGeojson.dataClassification.colors
  var ranges = addGeojson.dataClassification.ranges
  // var value = parseInt(feature.properties[addGeojson.dataName])
  return color[getClass(value, ranges)]
}

function getClass (val, a) {
  for (var i = 0; i < a.length; i++) {
    var item = a[i].split(/ - /)
    if (val <= parseFloat(item[1])) {
      return i
    }
  }
}

// function pointFilter (feature) {
//   if (feature.geometry.type === 'Point') return true
// }
