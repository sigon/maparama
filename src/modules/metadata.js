// src/modules/metadata.js
var $ = require('jquery')
/* global M */
var metadata = {}

metadata.content = {}
metadata.get = function () {
  return this.content
}
metadata.put = function (url, map) {
  $.getJSON(url, function (data) {
    this.content = data
    M.mapInfo.mapOptions = {
      title: data.name,
      attribution: data.attribution,
      sourceLink: 'https://' + data.sourceDomain + '/resource/' + data.id + '.json',
      lastUpdated: data.lastUpdated,
      columns: data.columns
    }
    M.mapInfo.addData()
    M.mapInfo.addTo(map)
  })
}

module.exports = metadata
