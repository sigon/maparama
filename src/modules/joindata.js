// src/joindata.js
// Join two files with a common attribute
var $ = require('jquery')

var joinData = {
  joinFile: {},
  targetFile: {},
  map: {},
  options: {}
}

// receive: target geojson file, join json data and common attribute name
// returns: expanded geojson
joinData.attributeJoinData = function (options, done) {
  var extendedGeojson = dataloop(options)
  done(extendedGeojson)
}

// receive: target geojson URL, join json URL and common attribute name
// returns: expanded geojson
joinData.attributeJoinURL = function (options, done) {
  var joinFile = $.getJSON(options.joinFile.url)
  var targetFile = $.getJSON(options.targetFile.url)

  $.when(joinFile, targetFile).done(function (join, target) {
    options.joinFile.data = join[0]
    options.targetFile.data = target[0]
    var extendedGeojson = dataloop(options)
    done(extendedGeojson)
  })
}

module.exports = joinData

// some times attributes are the same but are in uppercase or has accents
// like: 'Ávila' and 'AVILA'
// getCleanedString() source:
// https://retro.relaxate.com/tutorial-javascript-limpiar-cadena-acentos-tildes-extranos/
function getCleanedString (cadena) {
  var specialChars = '!@#$^&%*()+=-[]/{}|:<>?,.'
  for (var i = 0; i < specialChars.length; i++) {
    cadena = cadena.replace(new RegExp('\\' + specialChars[i], 'gi'), '')
  }
  cadena = cadena.toLowerCase()
  cadena = cadena.replace(/ /g, '_')
  cadena = cadena.replace(/á/gi, 'a')
  cadena = cadena.replace(/é/gi, 'e')
  cadena = cadena.replace(/í/gi, 'i')
  cadena = cadena.replace(/ó/gi, 'o')
  cadena = cadena.replace(/ú/gi, 'u')
  cadena = cadena.replace(/ñ/gi, 'n')
  return cadena
}

function dataloop (options) {
  var joinColumn = options.joinFile.joinColumn
  var extendedGeojson = options.targetFile.data
  var joinData = options.joinFile.data
  var joinFieldT // name of the attribute to join on target
  var joinFieldJ // name of the attribute to join on joinData

  // loop throught geojson and json to find common attribute
  // creates a new field in features.properties (joinColumn) with join data
  for (var i in extendedGeojson.features) {
    joinFieldT = extendedGeojson.features[i].properties[options.targetFile.attrField]
    joinFieldT = getCleanedString(joinFieldT)

    for (var ii in joinData) {
      joinFieldJ = joinData[ii][options.joinFile.attrField]
      joinFieldJ = getCleanedString(joinFieldJ)

      if (joinFieldJ === joinFieldT) {
        var value = joinData[ii][joinColumn]
        extendedGeojson.features[i].properties[joinColumn] = value
      }
    }
  }
  return extendedGeojson
}
