// src/modules/mapInfo.js
// Map Info (title and data source)
var L = require('leaflet')
var $ = require('jquery')
/* global M */

var mapInfo = L.control({ position: 'bottomright' })

mapInfo.mapOptions = {
  title: 'Título del mapa',
  attribution: 'Fuente de los datos',
  sourceLink: '',
  color: 'black',
  lastUpdated: '',
  columns: {}
}

mapInfo.addMapInfo = function (options) {
  mapInfo.addData(options)
  mapInfo.addTo(M.mapObject)
}
mapInfo.onAdd = function () {
  var div = L.DomUtil.create('div', 'mapInfo-ctrl')

  div.innerHTML += '<div><strong>' + this.mapOptions.title + '</strong></div>'
  div.innerHTML += '<div>Datos actualizados por última vez: ' + getTime(this.mapOptions.lastUpdated) + '</div>'
  div.innerHTML += '<div><small class="mapInfo-ctrl-source">Fuente: <a href=' +
    this.mapOptions.sourceLink + '>' + this.mapOptions.attribution + '</a></small></div>'
  return div
}
mapInfo.addColor = function () {
  if (this.mapOptions.color) {
    $('.mapInfo-ctrl').css({ 'color': this.mapOptions.color })
    $('.geostats-legend').css({ 'color': this.mapOptions.color })
  }
}
mapInfo.addData = function (options) {
  if (options && options.title) { this.mapOptions.title = options.title }
  if (options && options.sourceName) { this.mapOptions.attribution = options.sourceName }
  if (options && options.sourceLink) { this.mapOptions.sourceLink = options.sourceLink }
  if (options && options.color) { this.mapOptions.color = options.color }
}
module.exports = mapInfo

function getTime (timestamp) {
  var date = new Date(timestamp * 1000)
  var dateStr = date.toLocaleString('es-ES')
  return dateStr
}
