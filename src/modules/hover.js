// src/modules/hover.js
// highlight feature on hover and show info on a fixed modal
/* global M */
var featureInfo = require('./featureInfo')
var hover = {}

hover.highlightFeature = function (e) {
  var layer = e.target

  layer.setStyle({
    weight: 5,
    dashArray: '',
    fillOpacity: 0.7
  })
  if (M.featureInfoAction === 'onhover') {
    featureInfo.info.addTo(M.mapObject)
    featureInfo.update(e)
  }
}

hover.resetHighlight = function (e) {
  M.geojsonObject.resetStyle(e.target)
  if (M.featureInfoAction === 'onhover') {
    featureInfo.update(e)
  }
}

module.exports = hover
