// src/modules/charts.js
var c3 = require('c3')
var $ = require('jquery')
/* global M */
var chart = {
  svgChart: {}
}

chart.generateChart = function (e) {
  $('#myModal').show()
  var ts = M.timeSeries
  var spatialField = e.target.feature.properties[ts.spatialField]
  var chartColumns = getChartColumns(e.target.feature.properties[ts.choroplethData])
  var chart = c3.generate({
    bindto: '#chart',
    data: {
      x: ts.timeField,
      xFormat: '%Y', // 'xFormat' can be used as custom format of 'x'
      columns: chartColumns,
      onclick: function (e) {
        M.timeSeries.current = M.timeSeries.arr[e.index]
        M.geojson.updateTimeSeries()
      }
    },
    size: {
      height: 200
    },
    title: {
      text: ts.choroplethDataName + ' - ' + spatialField
    },
    axis: {
      x: {
        type: 'timeseries',
        tick: {
          // format: '%Y-%m-%d'
          format: '%Y'
        }
      }
    }
  })

  this.svgChart = chart
  $('.close-chart').click(function () {
    console.log('click! span')
    $('#myModal').hide()
  })
}

chart.loadChart = function (e) {

}

module.exports = chart

function getChartColumns (data) {
  var ts = M.timeSeries
  var xCol = []
  var yCol = [ts.choroplethData]
  var timeField = ''
  for (var i in data) {
    yCol.push(data[i][ts.choroplethData])
    timeField = data[i][ts.timeField]
    xCol.push(timeField.toString())
  }
  xCol.unshift(ts.timeField)

  var columns = []
  columns.push(xCol)
  columns.push(yCol)
  return columns
}
