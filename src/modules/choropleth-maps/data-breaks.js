// src/modules/data-breaks.js
var Geostats = require('geostats')
var chroma = require('chroma-js')
var $ = require('jquery')
/* global M */

var dataBreaksModule = {
  dataBreaks: [],
  getBreaks: function (features, options, callback) {
    var numDataBreaks = options.numDataBreaks ? options.numDataBreaks : 4
    var field = options.choroplethData
    var choroplethDataName = options.choroplethDataName ? options.choroplethDataName : field

    var valuesArray = [] // series data
    if (options.timeseries) {
      var currentTime = M.timeSeries.current
      for (var ii = 0; ii < features.length; ii++) {
        var timeseries = features[ii].properties[field]
        for (var iii = 0; iii < timeseries.length; iii++) {
          var cvalue = parseInt(timeseries[iii][options.timeseries.timeField])
          if (cvalue === currentTime) {
            valuesArray.push(timeseries[iii][options.choroplethData])
          }
        }
      }
    } else {
      for (var i = 0; i < features.length; i++) {
        var value = parseInt(features[i].properties[field])
        if (!isNaN(value)) {
          valuesArray.push(value)
        }
      }
    }
    var serie = new Geostats(valuesArray)
    console.log('MIN: ' + serie.min())
    console.log('MAX: ' + serie.max())

    // var numDataBreaks = options.numDataBreaks
    switch (options.classification) {
      case 'Quantile':
        serie.getClassQuantile(numDataBreaks)
        break
      case 'Jenks':
        serie.getClassJenks(numDataBreaks)
        break
      case 'EqInterval':
        serie.getClassEqInterval(numDataBreaks)
        break
      default:
        serie.getClassJenks(numDataBreaks)
    }
    var cBrewer = options.colorBrewer ? options.colorBrewer : 'OrRd'
    var colors = chroma.scale(cBrewer).colors(numDataBreaks)
    serie.setColors(colors)

    console.log(serie.method)
    console.log('Counter: ' + serie.counter)
    console.log('Ranges: ' + serie.ranges)
    // getHtmlLegend(colors, legend, counter, callback, mode, order)
    // mode: null, 'default', 'distinct', 'discontinuous'
    // order: null, 'ASC', 'DESC'

    var geostatsLegendTitle
    if (!$.isEmptyObject(M.timeSeries)) {
      geostatsLegendTitle = choroplethDataName + '-' + M.timeSeries.current
    } else {
      geostatsLegendTitle = choroplethDataName
    }
    var dataClassification = {
      breaks: serie.bounds,
      colors: serie.colors,
      legend: serie.getHtmlLegend(null, geostatsLegendTitle),
      ranges: serie.ranges
    }
    callback(dataClassification)
  }
}

module.exports = dataBreaksModule
