// src/modules/modal.js
// Modal window inside your Leaflet map.
// https://www.npmjs.com/package/leaflet-modal
require('leaflet-modal')
var $ = require('jquery')
/* global M */

// var timeSeries = require('./timeseries')

var modalModule = {
  leafletModal: {},
  addTemplate: function (leafletModalcustom) {
    this.leafletModal = leafletModalcustom
  },
  useTemplate: function (e, featProperties) {
    var leafletModalParams = {}
    var leafletModal = modalModule.leafletModal
    for (var key in leafletModal) {
      if (leafletModal.hasOwnProperty(key)) {
        var field = leafletModal[key]
        var value = featProperties[field] ? featProperties[field] : ''
        leafletModalParams[key] = value
      }
    }
    leafletModalParams['template'] = leafletModal.template
    e.target._map.fire('modal', leafletModalParams)
  },
  getTimeSeriesModal: function (e) {
    var tSeries = M.timeSeries
    var featProperties = e.target.feature.properties
    var content = ''
    for (var field in featProperties) {
      var fieldName = getFieldName(field)
      if (field === tSeries.choroplethData) {
        content += 'fecha: ' + tSeries.current + '<br/>'
        var index = getArrayIndexForKey(
          featProperties[field],
          tSeries.timeField,
          tSeries.current)
        content += fieldName + ': ' + featProperties[field][index][tSeries.choroplethData]
      } else {
        content += fieldName + ': ' + featProperties[field] + '<br/>'
      }
    }
    return content
  },
  fireTimeSeries: function (e) {
    var content = this.getTimeSeriesModal(e)
    e.target._map.fire('modal', { content: content })
    // M.chart.generateChart(e)
    // M.chart.loadChart(e)
  },
  fire: function (e) {
    if ($.isEmptyObject(M.timeSeries)) {
      var featProperties = e.target.feature.properties
      // user didnt define a leaflet modal template:
      if ($.isEmptyObject(modalModule.leafletModal)) {
        var content = ''
        for (var field in featProperties) {
          var fieldName = getFieldName(field)
          content += fieldName + ': ' + featProperties[field] + '<br/>'
        }
        e.target._map.fire('modal', { content: content })
      // use the custom leaflet template defined by user:
      } else {
        modalModule.useTemplate(e, featProperties)
      }
    } else {
      modalModule.fireTimeSeries(e)
    }
  }
}

module.exports = modalModule

function getFieldName (field) {
  var columns = M.mapInfo.mapOptions.columns
  if (columns.length > 1) {
    var index = getArrayIndexForKey(columns, 'fieldName', field)
    // check if colum in GeoJSON does not exits in data field after join
    var fName = index === -1 ? field : columns[index].name
    return fName
  } else {
    return field
  }
}

//  https://stackoverflow.com/questions/19111224/get-index-of-array-of-objects-via-jquery
function getArrayIndexForKey (arr, key, val) {
  for (var i = 0; i < arr.length; i++) {
    if (arr[i][key] === val) {
      return i
    }
  }
  return -1
}
