// modules/basemaps.js
var L = require('leaflet')

// https://leaflet-extras.github.io/leaflet-providers/preview/

// https://{s}.basemaps.cartocdn.com/{style}/{z}/{x}/{y}{scale}.png
// carto:{
//   styles:{
//     light_all,
//     dark_all,
//     light_nolabels,
//     light_only_labels,
//     dark_nolabels,
//     dark_only_labels,
//     rastertiles/voyager,
//     rastertiles/voyager_nolabels,
//     rastertiles/voyager_only_labels,
//     rastertiles/voyager_labels_under
//   }
// }

var basemaps = {
  maxZoom: 19,
  service: {
    carto: {
      tilesServiceURL: 'https://{s}.basemaps.cartocdn.com/',
      tilesSubZoomScale: '/{z}/{x}/{y}{r}.png',
      tilesURL: 'https://{s}.basemaps.cartocdn.com/light_all/{z}/{x}/{y}{r}.png',
      attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a> &copy; <a href="https://carto.com/attributions">CARTO</a>',
      subdomains: 'abcd'
    },
    osm: {
      tilesURL: 'https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
      attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors',
      subdomains: 'abcd'
    }
  }
}

basemaps.addBasemap = function (map, options) {
  if (!options) { options = { service: 'osm' } } // default basemap (OSM)
  if (options.service === 'carto' && options.style) {
    var carto = this.service.carto
    var cartoTiles = carto.tilesServiceURL + options.style + carto.tilesSubZoomScale
    this.service.carto.tilesURL = cartoTiles
  }

  L.tileLayer(this.service[options.service].tilesURL, {
    maxZoom: 19,
    subdomains: this.service[options.service].subdomains,
    attribution: this.service[options.service].attribution
  }).addTo(map)
}

module.exports = basemaps
