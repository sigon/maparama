var L = require('leaflet')
var $ = require('jquery')
/* global M */

var burgosMapsLogo = L.control({ position: 'bottomleft' })

burgosMapsLogo.addLogo = function (link, url) {
  this.addTo(M.mapObject)
  this.update(link, url)
}
burgosMapsLogo.onAdd = function () {
  var div = L.DomUtil.create('div', 'burgosMaps-ctrl')
  div.innerHTML += '<a class="burgosMaps-logo" target="_blank" href="https://burgos-maps.gitlab.io/" alt="Burgos Maps logo"></a>'
  return div
}

// This function change the default logo icon and the href
burgosMapsLogo.update = function (href, logoUrl) {
  if (href) {
    $('.burgosMaps-logo').attr('href', href)
  }
  if (logoUrl) {
    var urlProperty = 'url("' + logoUrl + '")'
    $('.burgosMaps-logo').css({ 'background-image': urlProperty })
  }
}

module.exports = burgosMapsLogo
