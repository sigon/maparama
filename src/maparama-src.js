// src/maparama-src.js
var L = require('leaflet')
var $ = require('jquery')

// Maparama modules
var logo = require('./modules/logo')
var mapInfo = require('./modules/mapInfo')
var basemaps = require('./modules/basemaps')
var hoverModule = require('./modules/hover')
var featureInfo = require('./modules/featureInfo')
var geojson = require('./modules/add-geojson')
var metadata = require('./modules/metadata')
var modal = require('./modules/modal')
var joinData = require('./modules/joindata')
var chartModule = require('./modules/charts')

L.Icon.Default.imagePath = '../lib/leaflet-1.4.0/images/'

var maparama = {
  mapObject: {}, // leaflet map object
  timeSeries: {}, // maparama object with timeseries info
  geojsonObject: {}, // leaflet GeoJSON layer
  featureInfoAction: 'popup' // default
}

maparama.logo = logo
// maparama.addLogo = function (link, url) {
//   logo.addTo(this.mapObject)
//   logo.update(link, url)
// }

maparama.mapInfo = mapInfo

maparama.geojson = geojson

// maparama.addGeojson = function (geojsonData, options) {
//   geojson.addFile(this.mapObject, geojsonData, options)
// }
// maparama.addGeojsonByURL = function (geojsonURL, options) {
//   geojson.getFile(this.mapObject, geojsonURL, options)
// }

maparama.getMap = function () {
  return this.mapObject
}
maparama.getTimeSeries = function () {
  return this.timeSeries
}
maparama.addBasemap = function (options) {
  basemaps.addBasemap(this.mapObject, options)
}
maparama.addMetadata = function (url) {
  metadata.put(url, this.mapObject)
}
maparama.modalTemplate = function (options) {
  modal.addTemplate(options)
}
maparama.chart = chartModule
maparama.hover = hoverModule
maparama.featureInfo = featureInfo

maparama.joinData = function (options, done) {
  $('#mySpinner').addClass('spinner')

  if (options.targetFile.data && options.joinFile.data) {
    joinData.attributeJoinData(options, function (data) {
      done(data)
    })
  // get json URL:
  } else {
    joinData.attributeJoinURL(options, function (data) {
      done(data)
    })
  }
}
maparama.jquery = function () {
  return $
}

module.exports = maparama
