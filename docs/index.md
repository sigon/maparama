
# maparama.js

**maparama.js** a library that helps **me** to create thematic maps.

## Quick-start

You can add directly a geoJSON file that will display on a Leaflet map, if you want a thematic map with some statistical data you can customize the choropleth map with a few params.

Features:

* add directly a geoJSON file (local or url)
* choose the data classification (Jenks, Quantile, Equal Interval)
* choose the color squeme
* add title and a legend
* even add your own logo

This library internally has this dependencies:

```
"dependencies": {
  "@turf/turf": "^5.1.6",
  "chroma-js": "^2.0.3",
  "geostats": "^1.7.0",
  "jquery": "^3.3.1",
  "leaflet": "^1.4.0",
  "leaflet-modal": "^0.1.5"
}
```

- [LeafletJS](https://leafletjs.com/) is an open-source JavaScript library for interactive maps.
- [GeostatsJS](https://github.com/simogeo/geostats) is a JS library for data classification.
- [TurfJS](https://turfjs.org/) for advanced geospatial analysis
- [Chroma-js](https://github.com/gka/chroma.js) is JS library for color conversions and color scales

Here's an example:

```js
var M = maparama
M.mapObject = L.map('map').setView([42.344335, -3.69380950], 14)
M.addBasemap({ service: 'carto', style: 'dark_all' })

var geojsonURL = './data/polygons.geojson'
M.addGeojsonByURL(geojsonURL, {
  choroplethData: 'pop2015', // name of the geojson propertie
  choroplethDataName: 'Population in 2015', // legend
  numDataBreaks: 4,
  classification: 'Jenks', // ['Jenks', 'Quantile', 'EqInterval']
  colorBrewer: 'BuGn'
})

// M.addGeojson(geojsonURL)
M.addLogo()
M.addMapInfo({
  title: 'Este es el Título del mapa',
  sourceName: 'source whatever.es',
  sourceLink: 'sourcelink.es'
})
```

## API

| Method        |        |
| ------------- |:-------------:|
|[.addBasemap()](#addbasemap) | [.modalTemplate()](#modaltemplate)|
|[.addMapInfo()](#addmapinfo)| [.joinData()](#joindata) |
|[.addMetadata()](#addmetadata)| |
|[.addLogo()](#addlogo)||
|[.addGeojsonByURL()](#addgeojsonbyurl) | [.addGeoJson()](#addgeojson)|


### addBasemap()

**options**:

{**service**\<string\>?, **style**\<string\>?}

This method add a basemap. Available basemaps are:
- osm
- carto (styles:['light_all', 'dark_all', 'rastertiles/voyager'])

```js
M.addBasemap()
M.addBasemap({service:'osm'})
M.addBasemap({ service: 'carto', style: 'light_all' })
M.addBasemap({ service: 'carto', style: 'dark_all' })
```

### addMapInfo()

**options**:

{**title**\<string\>, **sourceName**\<string\>, **sourceLink**\<string\>, **color**\<string\>?}

This will add the map title and the data source. If the chosen basemap is dark, you can pass the text color so the title remain visible.

```js
M.addMapInfo({
  title: 'THIS IS THE MAP TITLE',
  sourceName: 'source whatever',
  sourceLink: 'sourcelink.es',
  color: 'white'
})
```

### addLogo()

**options**:

**href**\<string\>?, **logoUrl**\<string\>?}

If not href or logo url the default **Burgos Maps** logo will be placed

```js
M.addLogo()
M.addLogo('supercoolwebpage.com', 'supercoolwebpage.com/logo.icon')
```

### addGeojsonByURL()

**options**:

geojsonUrl\<string\>{**choroplethData**\<string\>, **choroplethDataName**\<string\>?, **numDataBreaks**\<number\>?, **classification**\<string\>?, **colorBrewer**\<string\>?}

You can pass the name of the field on the geoJSON file that has the values that you want to display on the thematic map ```choroplethData:'pop2015'```. If you want to change the name of the value for the legend use ```choroplethDataName:'Population in 2015'```.

To select the color squeme for the map use the ```colorBrewer``` option, it use the [ColorBrewer scales](http://colorbrewer2.org/#type=sequential&scheme=BuGn&n=3), you can put there any brewer scale:
- secuential **BuGn** (blue-green), **OrRd** (orange-red), **Oranges** or
- diverging like **BrBG**

For data classification you can select the classification scheme depending on the type statistical distribution of the data. Available method are:
- Jenks (default, for variable class width)
- Quantiles (each class contains a similar number of units)
- Equal Interval (for uniform distributions)

```js
var geojsonURL = './data/polygons.geojson'
M.addGeojsonByURL(geojsonURL, {
  choroplethData: 'pop2015',
  choroplethDataName: 'Population in 2015',
  numDataBreaks: 4,
  classification: 'Jenks', // ['Jenks', 'Quantile', 'EqInterval']
  colorBrewer: 'BuGn' // ['BrBG', 'Oranges', 'Blues','OrRd', ..]
})
```

For a non choropleth map you can add just the geojson file:

```js
var geojsonURL = './data/polygons.geojson'
M.addGeojsonByURL(geojsonURL)
```

### addGeojson()

This method has the same structure as .addGeojsonByURL()(#addgeojsonbyurl), but instead of passing the GeoJSON file URL you can pass the GeoJSON object directly.

Used after .joinData() method to add to the map the extendedGeojson object that is returned.

### addMetadata()

**params**: metadataURL\<string\>

Given a metadata file in json format like this, to populate map title, attribution, and source link:

```js
{
  "id": "ft3a-5csh",
  "name": "Eventos de la agenda cultural categorizados y geolocalizados",
  "sourceDomain": "analisis.datosabiertos.jcyl.es",
  "attribution": "Junta de Castilla y Le\u00f3n",
  "lastUpdated": 1553674574,
  "columns": [
    {
      "name": "T\u00edtulo",
      "dataTypeName": "text",
      "fieldName": "t_tulo",
      "position": 1
    }]
}
```

The columns array will define the name displayed on the modal window for each field.

### modalTemplate()

This method use the [leaflet-modal](https://github.com/w8r/Leaflet.Modal) window handler.

If no leaflet-modal object is passed a default modal with all the columns will appear. User can define the modal content with a template, like this:

```js
var leafletModal = {
  title: 't_tulo', // 't_tulo' is the name of the field in the geojson file
  content: 'descripci_n',
  enlace: 'enlacecontenido',
  template: ['<div class="modal-header"><h2>{title}</h2></div>',
    '<hr>',
    '<div class="modal-body">{content}</div>',
    '<div class="modal-footer"><a href="{enlace}" title="">Enlace</a></div>'
  ].join('')
}

M.modalTemplate(leafletModal)
```

### joinData()

This method accepts as params two files, one containing the spatial info (.geojson), here the ```targetFile```, and the other, called ```joinFile```, containing some statistical data (.json). Both must have some common attribute (```attrField```) to do the join.

joinData() returns a geojson object (the original geospatial file, with one more field (```joinColumn```) added on properties).

joinData\<object\>{**joinFile**\<object\>, **targetFile**\<object\>?}

In the options, user can pass the data URL or json data directly: ```joinFile.url``` or ```joinFile.data```

```js
M.joinData({
  'joinFile': {
    'url': dataURL, 'attrField': 'provincia', 'joinColumn': 'nacimientos'
  },
  'targetFile': {
    'url': geojsonURL, 'attrField': 'provincia'
  }
}, function (extendedGeojson) {
  M.addGeojson(extendedGeojson, choroplethMapOptions)
})
```

Usage example:

GeoJSON with the provincias of CyL

```js
  "type": "FeatureCollection",
  "features": [
    {"type":"Feature",
     "properties":{"pr":"05","provincia":"Ávila"},
      "geometry":{"type":...}
    },
    ...
```

JSON with 'Número de nacimientos por provincias, CyL año 2015'

```js
[{"a_o":"2015-01-01T00:00:00.000","nacimientos":"1107","provincia":"AVILA"}
,{"a_o":"2015-01-01T00:00:00.000","nacimientos":"2760","provincia":"BURGOS"}
,{"a_o":"2015-01-01T00:00:00.000","nacimientos":"3026","provincia":"LEON"}
,{"a_o":"2015-01-01T00:00:00.000","nacimientos":"1162","provincia":"PALENCIA"}
,{"a_o":"2015-01-01T00:00:00.000","nacimientos":"2342","provincia":"SALAMANCA"}
,{"a_o":"2015-01-01T00:00:00.000","nacimientos":"1197","provincia":"SEGOVIA"}
,{"a_o":"2015-01-01T00:00:00.000","nacimientos":"634","provincia":"SORIA"}
,{"a_o":"2015-01-01T00:00:00.000","nacimientos":"4192","provincia":"VALLADOLID"}
,{"a_o":"2015-01-01T00:00:00.000","nacimientos":"969","provincia":"ZAMORA"}]
```

joinData() add a new field in properties called 'nacimientos' ({joinColumn}) and return the ```extendedGeojson``` with nacimientos data added:

```js
  "type": "FeatureCollection",
  "features": [
    {"type":"Feature",
     "properties":{"pr":"05","provincia":"Ávila", "nacimientos": "1107"},
      "geometry":{"type":...}
    },
    ...
```

Both objects (geoJSON and JSON) must have some common attribute. Internally joinData() remove any special character and lower case the string. So:

```js
'Burgos' === 'BURGOS'
true
'Ávila' === 'AVILA'
true
```
